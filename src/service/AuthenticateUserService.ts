import axios from 'axios'
import { prismaClient } from '../prisma'
import { sign } from 'jsonwebtoken'

interface IAccessTokenResponse {
    access_token: string
}
interface IGithubUserResponse {
    avatar_url: string,
    login: string,
    id: number,
    name: string
}

class AuthenticateUserService {
    async execute(code: string) {
        const url = `https://github.com/login/oauth/access_token`
        const githubToken = await axios.post<IAccessTokenResponse>(url, null, {
            // get user access_token
            params: {
                client_id: process.env.GITHUB_CLIENT_ID,
                client_secret: process.env.GITHUB_CLIENT_SECRET,
                code
            },
            headers: {
                'Accept': 'application/json'
            }
        }).then(e => e.data.access_token)

        // get users data
        if(githubToken) {
            const url = `https://api.github.com/user`
            const githubUser = await axios.get<IGithubUserResponse>(url, {
                headers: {
                    authorization: `Bearer ${githubToken}`
                }
            }).then(e => e.data)

            // get user data from db
            const { id, login, name, avatar_url } = githubUser
            let user = await prismaClient.user.findFirst({where: {github_id: id}})
            if(!user) {
                user = await prismaClient.user.create({
                    data: {
                        github_id: id,
                        name,
                        avatar_url,
                        login
                    }
                })
            }
            const token = sign(
                {
                user: {
                    id: user.id,
                    name: user.name,
                    avatar: user.avatar_url
                }
            },
            process.env.JWT_TOKEN_SECRET as string,
            {
                subject: user.id,
                expiresIn: '1h'
            })

            return { token, user}
        } else {
            throw new Error('Unauthorized')
        }
    }
}

export { AuthenticateUserService }