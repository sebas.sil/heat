import { Request, Response } from 'express'
import {GetLastMessagesService} from "../service/GetLastMessagesService";

class GetLastMessagesController {
    async handle(request: Request, response: Response) {
        const service = new GetLastMessagesService()
        const ret = await service.execute(3)
        return response.json(ret)
    }
}

export { GetLastMessagesController }