import 'dotenv/config'
import express from 'express'
import { router } from './routes'
import http from 'http'
import { Server } from 'socket.io'
import cors from 'cors'

const app = express()
const server = http.createServer(app)
const io = new Server(server, {
    cors: {
        origin: '*'
    }
})

io.on('connect', (socket) => {
    console.log(`usuário conectado no socket ${socket.id}`)
})

app.use(cors())
app.use(express.json())
app.use(router)

app.get('/github', (req, res) => {
    res.redirect(`https://github.com/login/oauth/authorize?client_id=${process.env.GITHUB_CLIENT_ID}`)
})

app.get('/signin/callback', (req, res) => {
    const { code } = req.query
    return res.json(code)
})

export { server, io }