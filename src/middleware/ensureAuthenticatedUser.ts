import { Request, Response, NextFunction } from "express";
import { verify } from 'jsonwebtoken'

interface IPayload {
    sub: string
}

function ensureAuthenticated(request: Request, resposne: Response, next: NextFunction) {
    const authorization = request.headers.authorization

    if(authorization) {
        const [, token] = authorization.split(' ')
        try {
            const { sub } = verify(token, process.env.JWT_TOKEN_SECRET as string) as IPayload
            request.user_id = sub
            return next()
        } catch(err) {
            return resposne.status(401).json({error: 'expired token'})
        }
    } else {
        return resposne.status(401).json({error: 'not authenticated '})
    }
}

export { ensureAuthenticated }