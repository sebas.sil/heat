import { Request, Response } from 'express'
import {CreateMessageService} from "../service/CreateMessageService";

class CreateMessageController {
    async handle(request: Request, response: Response) {
        const user_id = request.user_id
        const { message } = request.body

        const service = new CreateMessageService()
        const ret = await service.execute(message, user_id)
        return response.json(ret)
    }
}

export { CreateMessageController }