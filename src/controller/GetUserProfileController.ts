import { Request, Response } from 'express'
import {GetLastMessagesService} from "../service/GetLastMessagesService";
import {GetUserProfileService} from "../service/GetUserProfileService";

class GetUserProfileController {
    async handle(request: Request, response: Response) {
        const user_id = request.user_id
        const service = new GetUserProfileService()
        const ret = await service.execute(user_id)
        return response.json(ret)
    }
}

export { GetUserProfileController }