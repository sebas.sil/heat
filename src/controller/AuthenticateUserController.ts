import { Request, Response } from 'express'
import { AuthenticateUserService } from "../service/AuthenticateUserService";

class AuthenticateUserController {
    async handle(request: Request, response: Response) {
        const { code } = request.body
        const service = new AuthenticateUserService()
        try {
            const ret = await service.execute(code as string)
            return response.json(ret)
        } catch (err) {
             return response.status(401).json({error: 'not authorized'})
        }
    }
}

export { AuthenticateUserController }