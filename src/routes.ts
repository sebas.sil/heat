import { Router } from 'express'
import {AuthenticateUserController} from "./controller/AuthenticateUserController";
import {ensureAuthenticated} from "./middleware/ensureAuthenticatedUser";
import {CreateMessageController} from "./controller/CreateMessageController";
import {GetLastMessagesController} from "./controller/GetLastMessagesController";
import {GetUserProfileController} from "./controller/GetUserProfileController";

const router = Router()

router.post('/authenticate', new AuthenticateUserController().handle)

router.post('/message', ensureAuthenticated, new CreateMessageController().handle)
router.get('/message', new GetLastMessagesController().handle)

router.get('/profile', ensureAuthenticated, new GetUserProfileController().handle)
export { router }